package com.boateng.abankus.database.configuration;

import java.util.concurrent.Semaphore;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PlainTextAuthProvider;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.ConstantReconnectionPolicy;
import com.datastax.driver.core.policies.DowngradingConsistencyRetryPolicy;

public class CassandraConfiguration {
	
	
	
	private Semaphore semaphore = new Semaphore(10,true);
	
	private Session session;

	private Cluster cluster;
	
	private static final CassandraConfiguration cassandra = new CassandraConfiguration ();
	
	public static CassandraConfiguration getInstance(){
		return cassandra;
	}
	
	private  CassandraConfiguration (){}
	
	private void connect(){
		cluster = Cluster.builder()
							
							.addContactPoint("localhost").withPort(9042)
							.withAuthProvider(new PlainTextAuthProvider("proteckadmin","Abankwah1"))
					         .withRetryPolicy(DowngradingConsistencyRetryPolicy.INSTANCE)
					         .withReconnectionPolicy(new ConstantReconnectionPolicy(100L))
							.build();
						
		session =  cluster.connect("protecktransaction");
	}
	public Session getSession() {
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try{
			connect();
			return this.session;
		}catch(Exception e){
			semaphore.release();
			return null;
		}finally{
			semaphore.release();
		}
	   }
	public void close() {
		   cluster.close();
		}
}
