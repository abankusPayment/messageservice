package com.boateng.abankus.database.configuration;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.retry.support.RetryTemplate;

import com.rabbitmq.client.Connection;

public class MessagingConfig {

	private static final String HELLO_WORLD_QUEUE_NAME = "paymentTransaction";
	Connection conn = null;
	
	private ExecutorService executor = Executors.newCachedThreadPool();
	
	private MessagingConfig(){}
	
	private static final MessagingConfig rabbit  = new MessagingConfig();
	
	@Autowired
	private ConnectionFactory connectionFactory;
	
	@Autowired
	private RetryTemplate retryTemplate;
	
	public static MessagingConfig getInstance(){
		return rabbit;
	}

	@Bean
	public SimpleRabbitListenerContainerFactory factory(){
		SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
		factory.setConcurrentConsumers(10);
		factory.setConnectionFactory(connectionFactory);
		factory.setMaxConcurrentConsumers(50);
		factory.setStartConsumerMinInterval(10000L);
		factory.setAutoStartup(true);
		factory.setTaskExecutor(executor);
		return factory;
		
	}
	@Bean
	public Queue helloWorldQueue() {
	    return new Queue(HELLO_WORLD_QUEUE_NAME);
	}
	
	@Bean
	public AmqpTemplate rabbitTemplate() {
	    RabbitTemplate template = new RabbitTemplate();
	    template.setConnectionFactory(connectionFactory);
	    template.setRoutingKey("payment");
	    template.setRetryTemplate(retryTemplate);
	    return template;
	}
	
	@Bean
	public SimpleMessageListenerContainer listenerContainer() {
	    SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
	    container.setConnectionFactory(connectionFactory);
	    container.setQueues(helloWorldQueue());
	    container.setMessageListener(exampleListener() );
	    return container;
	}
    @Bean
    public MessageListener exampleListener() {
        return new MessageListener() {

			@Override
			public void onMessage(Message message) {
				 System.out.println("received: " + message);
				
			}
        };
    }
	/**
	public Connection getConnection(String username, String password, String host, int port){
		
		try{
		ConnectionFactory factory = new ConnectionFactory();
		factory.setUsername(username);
		factory.setPassword(password);
		factory.setHost( host);
		factory.setPort(port);
		
		//When connection is lost, the fallback
		factory.setAutomaticRecoveryEnabled(true);
		factory.setNetworkRecoveryInterval(10000);
		//Creating Rabbit Connection
		conn = factory.newConnection();
		
		
		}catch(Exception e){
		}
		return conn;
	}
	
	/**
	 * Method closes MEssaging Connection if connection 
	 * is open.
	 * @throws IOException
	 */
	public void closeConnection() throws IOException{
		if(conn.isOpen()){
			conn.close();
		}
	}
	
	/**
	 * This methods aborts the Connection of
	 * RabbitMQ.
	 */
	public void abortConnection(){
		if(conn.isOpen()){
			conn.abort(100);
		}
	}
	
}
