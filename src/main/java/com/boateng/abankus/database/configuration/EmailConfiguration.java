package com.boateng.abankus.database.configuration;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

public class EmailConfiguration {

	private @Value("${email.port}") String emailServerPort;
	
	private @Value("${email.protocol}") String emailServerProtocol;
	
	private @Value("${email.username}") String emailServerUsername;
	
	private @Value("${email.host}") String emailServerHost;
	
	private @Value("${email.password}") String emailServerPassword;
	
	private @Value("${email.subject}") String emailSubject;
	
	@Bean
	public JavaMailSender mailSender() throws IOException {
	    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	    mailSender.setHost(emailServerHost);
	    mailSender.setPort(Integer.parseInt(emailServerPort));
	    mailSender.setProtocol(emailServerProtocol);
	    mailSender.setUsername(emailServerUsername);
	    mailSender.setPassword(emailServerPassword);
	    //mailSender.setJavaMailProperties(javaMailProperties());
	    return mailSender;
	}
}
