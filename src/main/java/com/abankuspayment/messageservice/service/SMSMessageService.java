package com.abankuspayment.messageservice.service;

import com.abankuspayment.messageservice.data.entity.SMSMessageRequest;

public interface SMSMessageService {
	
	void sendMessage(SMSMessageRequest request);
	
	void buildMessage (SMSMessageRequest request,String smsTemplate);

	void paymentNotification(SMSMessageRequest request);

	void customerNotification(SMSMessageRequest request);

	void customerPinNotification(SMSMessageRequest request);
}
