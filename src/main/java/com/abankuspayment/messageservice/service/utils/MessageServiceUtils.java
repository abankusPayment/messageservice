package com.abankuspayment.messageservice.service.utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.support.converter.SimpleMessageConverter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MessageServiceUtils {
    public static String convertObjectToJSON(Object object) throws JsonProcessingException{
    	String jsonString = null;
    	if(object instanceof Object){
    		ObjectMapper mapper = new ObjectMapper();
    		jsonString = mapper.writeValueAsString(object);
    	}
    	
    	return jsonString;
    }
	public static <T>  T convertFromJSON(Class<T> cls, String json) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		T type = null;
		if(json != null){
			type = mapper.readValue(json, cls);
		}
		
		return type;
	}
	
	public static <T> T convertFromJson(TypeReference<T> r, String json) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		T t = null;
		if(json != null){
			t = mapper.readValue(json, r);
		}
		return t;
		
	}
	public static String convertMessageToString(Message msg){
		SimpleMessageConverter convert = new SimpleMessageConverter();
		
		return convert.fromMessage(msg).toString();
	}
	
	public static Date convertStringToDate(String dateString,String dateFormat) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		Date dateTime = format.parse(dateString);
		
		return dateTime;

	}
}
