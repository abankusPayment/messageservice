package com.abankuspayment.messageservice.service;

public interface TransactionService<T> {
	
	boolean saveTransactionToCassandra(T t);

}
