package com.abankuspayment.messageservice.service;

public interface AuthenticationMessageService {

	void recieveForgotUsernameMessages(String message);
	
	void recieveForgotPasswordMessage(String  message);
	
	void changePasswordRequest(String message);

	void activateEmployeeRequest(String message);
}
