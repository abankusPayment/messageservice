package com.abankuspayment.messageservice.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.abankuspayment.messageservice.data.entity.CompanyNotificationMessage;
import com.abankuspayment.messageservice.data.entity.EmailMessageRequest;
import com.abankuspayment.messageservice.service.CompanyMessageService;
import com.abankuspayment.messageservice.service.EmailMessageService;
import com.abankuspayment.messageservice.service.utils.MessageServiceUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class CompanyMessageServiceImpl implements CompanyMessageService {

	@Autowired
	private EmailMessageService emailMessageServiceImpl;
	
	private @Value("${donotreply.emailAddress}") String donoteplyEmailAddress;
	
	private @Value("${companyRegistrationSubject}") String companyRegistrationSubject;
	
	private @Value("${companyRegistration.template}") String companyRegistrationtemplate;
	
	@Override
	public void processAndSendEmail(String message) throws JsonParseException, JsonMappingException, IOException {
		if(message != null){
			CompanyNotificationMessage companyRequest = MessageServiceUtils.convertFromJSON(CompanyNotificationMessage.class,message);
			EmailMessageRequest request = new EmailMessageRequest(companyRequest);
			emailMessageServiceImpl.buildCompanyRegistrationEmail(request, companyRegistrationtemplate);
			emailMessageServiceImpl.sendEmail(request, companyRegistrationSubject, donoteplyEmailAddress);
		}
		
	}

}
