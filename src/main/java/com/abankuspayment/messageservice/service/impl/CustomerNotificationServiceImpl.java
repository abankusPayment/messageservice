package com.abankuspayment.messageservice.service.impl;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import com.abankuspayment.messageservice.data.entity.SMSMessageRequest;
import com.abankuspayment.messageservice.service.TransactionService;
import com.boateng.abankus.database.configuration.CassandraConfiguration;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;

public class CustomerNotificationServiceImpl implements TransactionService<SMSMessageRequest>{
	
	private static final Logger logger = Logger.getLogger(CustomerNotificationServiceImpl.class.getName());
	
	private static final ExecutorService es = Executors.newCachedThreadPool();
	
	@Override
	public boolean saveTransactionToCassandra(SMSMessageRequest request) {
		es.submit(new Runnable(){
			public void run(){
				/**
				Session session = CassandraConfiguration.getInstance().getSession();
				PreparedStatement statement = session.prepare("Insert into \"customerNotification\" (\"paymentId\",\"companyId\",\"orderNumber\",\"amountPaid\",\"paymentDate\",\"employee\",\"productCode\",\"paymentMethod\") values (?,?,?,?,?,?,?,?)");
				BoundStatement bound = new BoundStatement(statement);
				session.execute(bound.bind(UUIDs.timeBased(),transaction.getCompanyId(),
								transaction.getOrderNumber(),transaction.getAmountPaid(),
								transaction.getPaymentDate(),transaction.getEmployee(),
								transaction.getProductCode(),transaction.getPaymentMethod())
							);
							**/
			}
		});
		return false;	
	}

}
