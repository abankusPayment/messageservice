package com.abankuspayment.messageservice.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.abankuspayment.messageservice.data.entity.SMSMessageRequest;
import com.abankuspayment.messageservice.service.SMSMessageService;
import com.abankuspayment.messageservice.service.utils.MessageFields;
import com.boateng.abankus.database.configuration.CassandraConfiguration;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;
import com.smsgh.ApiHost;
import com.smsgh.BasicAuth;
import com.smsgh.HttpRequestException;
import com.smsgh.Message;
import com.smsgh.MessageResponse;
import com.smsgh.MessagingApi;

public class SMSMessageServiceImpl implements SMSMessageService {

	private static final Logger logger = LoggerFactory.getLogger(SMSMessageServiceImpl.class.getName());
	
	private @Value("${smsClientId}") String clientId;
	
	private @Value("${smsSecretKey}") String secret;
	
	private @Value("${send_smsUrl}") String sendSMSUrl;
	
	private @Value("${customerPaymentNotification}") String customerPayment;
	
	private @Value("${newCustomerNotification}") String newCustomer;
	
	private @Value("${proteck}") String SMSFrom;
	
	@Autowired
	private VelocityEngine velocityEngine;
	
    public void setVelocityEngine(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }

	@Override
	public void sendMessage(SMSMessageRequest request) {
		logger.info("SMS Message: "+request.getUuid()+" has being sent");
		BasicAuth auth = new BasicAuth(clientId, secret);
        ApiHost host = new ApiHost(auth);
        // Instance of the Messaging API
        MessagingApi messagingApi = new MessagingApi(host);

        try {
            // Here we will send the message next week
            Message message = new Message();
            message.setContent(request.getSmsMessage());
            message.setFrom(SMSFrom);
            message.setTo(request.getPhoneNumberTo());
            message.setRegisteredDelivery(true);
            MessageResponse response = messagingApi.sendMessage(message);
           
            request.setSentMessageId(message.getMessageId());
            //saveMessage(request);
            logger.info("Server Response Status " + response.getStatus()+" Sent Message Id:"+request.getSentMessageId()+":"+ response.getClientReference());
        } catch (HttpRequestException ex) {
        	 logger.warn("Exception Server Response Status " + ex.getHttpResponse().getStatus());
        	 logger.warn("Exception Server Response Body " + ex.getHttpResponse().getBodyAsString());
        }
	}
	@Override
	public void buildMessage(SMSMessageRequest request,String smsTemplate) {
		try {
			Map<String,Object> model = new HashMap<String,Object>();
			model.put("companyName", request.getCompanyName());
			if(request.getRequestType().equalsIgnoreCase(customerPayment)){
				model.put("amountPaid", request.getAmountPaid());
				model.put("paymentDate", request.getPaymentDate());		
				model.put("customername",request.getCompanyAlias());
			}

			model.put("referenceCode",request.getReferenceCode());
			
			String message = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, smsTemplate,MessageFields.ENCODING, model);
			request.setPhoneNumberTo(request.getPhoneNumberTo());
			
			request.setSmsMessage(message);
			request.setCompanyName("Proteck");

		} catch (Exception e) {
			
		}
	}

	private void buildCustomerPinMessage(SMSMessageRequest request, String smsTemplate) {
		try {
			Map<String,Object> model = new HashMap<String,Object>();
			model.put("companyName", request.getCompanyName());
			model.put("pin", request.getCustomerPinCode());
			
			String message = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, smsTemplate,MessageFields.ENCODING, model);
			request.setPhoneNumberTo(request.getPhoneNumberTo());
			
			request.setSmsMessage(message);

		} catch (Exception e) {
		}
	}
	
	@SuppressWarnings("unused")
	private void saveMessage(SMSMessageRequest request){
		try{
			Session session = CassandraConfiguration.getInstance().getSession();
			PreparedStatement statement = session.prepare("Insert into \"customerNotification\" (\"notificationId\",\"companyId\",\"customerId\",\"paymentDate\","
					+ "\"paymentAmount\",\"phoneNumber\",\"message\",\"messageConfirmationId\",\"customerName\","
					+ "\"companyName\",\"employeeName\",\"referenceCode\",\"requestType\",\"sentMessageId\") values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			BoundStatement bound = new BoundStatement(statement);
			ResultSetFuture future = session.executeAsync(bound.bind(UUIDs.timeBased(),request.getCompanyId(),request.getCustomerId(),request.getPaymentDate(),
							request.getAmountPaid(),request.getPhoneNumberTo(),request.getSmsMessage(),request.getUuid(),"Hubert Boateng",request.getCompanyName(),
							request.getEmployee(),	request.getReferenceCode(),request.getRequestType(),""));
			if(future.isDone()){
				logger.info("Payment Confirmation to customer has being saved in Proteck Datawarehouse successfully.");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@Override
	public void paymentNotification(SMSMessageRequest request) {
		buildMessage(request,"paymentConfirmation.vm");
		//request.setPhoneNumberTo("+233200422310");
		sendMessage(request);
	}
	@Override
	public void customerNotification(SMSMessageRequest request) {
		buildMessage(request,"welcomeCustomer.vm");
		request.setPhoneNumberTo("+233244795514");
		sendMessage(request);
	}

	@Override
	public void customerPinNotification(SMSMessageRequest request) {
		buildCustomerPinMessage(request,"customerPinNotification.vm");
		//request.setPhoneNumberTo("+233244795514");
		sendMessage(request);
	}
}
