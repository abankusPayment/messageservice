package com.abankuspayment.messageservice.service.impl;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.abankuspayment.messageservice.data.entity.ChangePasswordRequest;
import com.abankuspayment.messageservice.data.entity.EmailMessageRequest;
import com.abankuspayment.messageservice.data.entity.EmployeeRegistrationRequest;
import com.abankuspayment.messageservice.data.entity.ForgotUsernameRequest;
import com.abankuspayment.messageservice.data.entity.MessageRequest;
import com.abankuspayment.messageservice.service.AuthenticationMessageService;
import com.abankuspayment.messageservice.service.EmailMessageService;
import com.abankuspayment.messageservice.service.utils.MessageServiceUtils;


public class AuthenticationMessageServiceImpl implements AuthenticationMessageService {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationMessageServiceImpl.class.getName());
	
	private ExecutorService executor = Executors.newCachedThreadPool();
	
	@Autowired
	private EmailMessageService emailMessageServiceImpl;
	
	private @Value("${forgotUsername.subject}") String forgotUsernameSubject;
	
	private @Value("${changePassword.subject}") String changePasswordSubject;
	
	private @Value("${forgotPassword.subject}") String forgotPasswordSubject;
	
	private @Value("${donotreply.emailAddress}") String donoteplyEmailAddress;
	
	@Override
	public void recieveForgotUsernameMessages(String message) {
		 executor.execute(new Runnable(){
			public void run(){
				if(message != null && !message.isEmpty()){
					
					ForgotUsernameRequest usernameRequest = null;
					try {
						usernameRequest = MessageServiceUtils.convertFromJSON(ForgotUsernameRequest.class,message);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(usernameRequest != null){
						EmailMessageRequest request = new EmailMessageRequest(usernameRequest);
						emailMessageServiceImpl.buildForgotPasswordEmail(request,"forgotUsername.vm");
						emailMessageServiceImpl.sendEmail(request,forgotUsernameSubject,donoteplyEmailAddress);
					}
					
				}				
			}
			
		});

		logger.info(message);
	}

	@Override
	public void recieveForgotPasswordMessage(String message) {
		 executor.execute(new Runnable(){

			@Override
			public void run() {
				if(message != null && !message.isEmpty()){
					
					ChangePasswordRequest usernameRequest = null;
					try {
						usernameRequest = MessageServiceUtils.convertFromJSON(ChangePasswordRequest.class,message);
						if(usernameRequest != null){
							EmailMessageRequest request = new EmailMessageRequest(usernameRequest);
							emailMessageServiceImpl.buildChangePassword(request,"forgotPassword.vm");
							emailMessageServiceImpl.sendEmail(request,forgotPasswordSubject,donoteplyEmailAddress);
						}
					} catch (IOException e) {
						logger.warn(e.getMessage(),e);
					}finally{
						usernameRequest = null;
					}

					
				}	
			}
			 
		 });
		
	}

	@Override
	public void changePasswordRequest(String message) {
		 executor.execute(new Runnable(){

				@Override
				public void run() {
					if(message != null && !message.isEmpty()){
						
						ChangePasswordRequest usernameRequest = null;
						try {
							usernameRequest = MessageServiceUtils.convertFromJSON(ChangePasswordRequest.class,message);
							if(usernameRequest != null){
								EmailMessageRequest request = new EmailMessageRequest(usernameRequest);
								emailMessageServiceImpl.buildChangePassword(request,"changePassword.vm");
								emailMessageServiceImpl.sendEmail(request,changePasswordSubject,donoteplyEmailAddress);
							}
						} catch (IOException e) {
							logger.warn(e.getMessage(),e);
						}finally{
							usernameRequest = null;
						}

						
					}	
				}
			 
		 });
	}

	@Override
	public void activateEmployeeRequest(String message) {
		executor.execute(new Runnable(){

			@Override
			public void run() {
				if(message != null && !message.isEmpty()){
					
					EmployeeRegistrationRequest employeeRequest = null;
					try {
						employeeRequest = MessageServiceUtils.convertFromJSON(EmployeeRegistrationRequest.class,message);
						if(employeeRequest != null){
							EmailMessageRequest request = new EmailMessageRequest(employeeRequest);
							emailMessageServiceImpl.buildChangePassword(request,"changePassword.vm");
							emailMessageServiceImpl.sendEmail(request,changePasswordSubject,donoteplyEmailAddress);
						}
					} catch (IOException e) {
						logger.warn(e.getMessage(),e);
					}finally{
						employeeRequest = null;
					}

					
				}	
			}
		 
	 });
	}

}
