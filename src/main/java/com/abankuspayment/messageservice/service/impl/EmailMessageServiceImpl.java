package com.abankuspayment.messageservice.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.abankuspayment.messageservice.data.entity.EmailMessageRequest;
import com.abankuspayment.messageservice.data.entity.MessageRequest;
import com.abankuspayment.messageservice.service.EmailMessageService;
import com.abankuspayment.messageservice.service.utils.MessageFields;

public class EmailMessageServiceImpl implements EmailMessageService {
	private static final Logger logger = LoggerFactory.getLogger(EmailMessageServiceImpl.class.getName());
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private VelocityEngine velocityEngine;
	
    public void setVelocityEngine(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }
    
	@Override
	public synchronized MessageRequest buildForgotPasswordEmail(EmailMessageRequest request,String emailTemplate) {
		Map<String,Object> model = null;
		String message = null;
		try{
			UUID uuid = UUID.randomUUID();
			logger.info("Validating and building email request to send. Message unique Id: "+uuid);
			
			request.setUuid(uuid);
			model = new HashMap<String,Object>();
			model.put("firstname", request.getReceiverName());
			model.put("username", request.getUsername());
			
			message = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, emailTemplate,MessageFields.ENCODING, model);
			request.setEmailMessage(message);
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}finally{
			model = null;
			message = null;			
		}

		return request;
	}

	@Override
	public synchronized void sendEmail(EmailMessageRequest request,String subject,String FromEmailAddress) {
		
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = null;
		try {
			helper = new MimeMessageHelper(message, false);
			helper.setTo(request.getEmailTo());
			helper.setSubject(subject);
			helper.setFrom(FromEmailAddress);
			helper.setText(request.getEmailMessage(), true);
			//Sending email
			mailSender.send(message);
		} catch (MessagingException e) {
			logger.warn(e.getMessage(), e);
		}
		

	}


	@Override
	public synchronized MessageRequest buildChangePassword(EmailMessageRequest messageRequest,String emailTemplate) {
		Map<String,Object> model = null;
		String message = null;
		try{
			UUID uuid = UUID.randomUUID();
			logger.info("Validating and building email request to send. Message unique Id: "+uuid);
			
			messageRequest.setUuid(uuid);
			model = new HashMap<String,Object>();
			model.put("user", messageRequest.getReceiverName());
			model.put("emailLink", messageRequest.getUrl());
			model.put("tempPassword", messageRequest.getTempPassword());
			message = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, emailTemplate,MessageFields.ENCODING, model);
			messageRequest.setEmailMessage(message);
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}finally{
			model = null;
			message = null;			
		}

		return messageRequest;
	}

	@Override
	public MessageRequest buildCompanyRegistrationEmail(EmailMessageRequest request, String emailTemplate) {
		Map<String,Object> model = null;
		String message = null;
		try{
			UUID uuid = UUID.randomUUID();
			logger.info("Validating and building email request to send. Message unique Id: "+uuid);
			
			request.setUuid(uuid);
			model = new HashMap<String,Object>();
			model.put("companyName", request.getCompanyName());
			
			message = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, emailTemplate,MessageFields.ENCODING, model);
			request.setEmailMessage(message);
			logger.info(request.getReceiverName());
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}finally{
			model = null;
			message = null;			
		}

		return request;
	}


}
