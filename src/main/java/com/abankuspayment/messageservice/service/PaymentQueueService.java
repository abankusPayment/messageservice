package com.abankuspayment.messageservice.service;

public interface PaymentQueueService {
	
	/**
	 * Build and sends queue message to AWS SQS
	 * 
	 * @param queuename
	 * @return
	 */
	boolean buildAndSendMessage(String queuename);
	
	/**
	 * Retrieve queued message from AWS SQS for every 3 minutes. If there
	 * are no queued messages, it returns 
	 * @param queuename
	 * @return
	 */
	boolean retrieveMessage(String queuename);
}
