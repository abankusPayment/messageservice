package com.abankuspayment.messageservice.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface CompanyMessageService {

	void processAndSendEmail(String message) throws JsonParseException, JsonMappingException, IOException;

	
}
