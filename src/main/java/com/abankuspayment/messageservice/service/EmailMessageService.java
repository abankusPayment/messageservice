package com.abankuspayment.messageservice.service;

import com.abankuspayment.messageservice.data.entity.EmailMessageRequest;
import com.abankuspayment.messageservice.data.entity.MessageRequest;

public interface EmailMessageService {

	MessageRequest buildForgotPasswordEmail(EmailMessageRequest meessageRequest,String emailTemplate);
	
	void sendEmail(EmailMessageRequest meessageRequest, String forgotUsernameSubject, String fromEmailAddress);

	MessageRequest buildChangePassword(EmailMessageRequest meessageRequest, String emailTemplate);
	
	MessageRequest buildCompanyRegistrationEmail(EmailMessageRequest meessageRequest, String emailTemplate);
}
