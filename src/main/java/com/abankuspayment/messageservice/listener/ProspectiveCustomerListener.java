package com.abankuspayment.messageservice.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

/**
 * Use this listener for all messaging pertaining to ProSalesman
 * Prospective Customer
 * 
 * @author abankwah
 *
 */
public class ProspectiveCustomerListener implements MessageListener {

	private static final Logger logger = LoggerFactory.getLogger(ProspectiveCustomerListener.class.getName());
	
	@Override
	public void onMessage(Message message) {
		// TODO Auto-generated method stub
		
	}
	
	public void handleProspectiveRegistration(String message){
		
	}
	
	/**
	 * Use to send sms or email notification when an agent has being
	 * assigned to Customer
	 * 
	 * @param message
	 */
	public void handleProspectiveAssignedtoAgent(String message){
		
	}


}
