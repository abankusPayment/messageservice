package com.abankuspayment.messageservice.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.abankuspayment.messageservice.service.AuthenticationMessageService;
import com.abankuspayment.messageservice.service.utils.MessageServiceUtils;

public class ChangePasswordMessageListener   implements MessageListener{

	private static final Logger logger = LoggerFactory.getLogger(ChangePasswordMessageListener.class.getName());
	
	@Autowired
	private AuthenticationMessageService authenticationMessageServiceImpl;
	
	@Override
	public void onMessage(Message message) {
		if(message != null){
			String msg = MessageServiceUtils.convertMessageToString(message);
			logger.info(msg);
			handleMessage(msg);
		}
	}
	private void handleMessage(String msg) {
		authenticationMessageServiceImpl.changePasswordRequest(msg);
		
	}

}
