package com.abankuspayment.messageservice.listener;

import org.apache.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.abankuspayment.messageservice.service.AuthenticationMessageService;
import com.abankuspayment.messageservice.service.utils.MessageServiceUtils;

public class AuthenticationMessageListener  implements MessageListener{

	private static final Logger logger = Logger.getLogger(AuthenticationMessageListener.class);
	
	@Autowired
	private AuthenticationMessageService authenticationMessageServiceImpl;
	
	public void handleForgotUsernameMessage(String message){
		authenticationMessageServiceImpl.changePasswordRequest(message);
	}

	@Override
	public void onMessage(Message message) {
		if(message != null){
			String msg = MessageServiceUtils.convertMessageToString(message);
			handleForgotUsernameMessage(msg);
		}
	}
	
}
