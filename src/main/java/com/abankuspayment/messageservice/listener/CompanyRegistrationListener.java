package com.abankuspayment.messageservice.listener;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.abankuspayment.messageservice.service.CompanyMessageService;
import com.abankuspayment.messageservice.service.utils.MessageServiceUtils;

public class CompanyRegistrationListener  implements MessageListener {

	private static final Logger logger = LoggerFactory.getLogger(CompanyRegistrationListener.class);
	
	@Autowired
	private CompanyMessageService companyMessageServiceImpl;
	
	@Override
	public void onMessage(Message message) {
		if(message != null){
			String msg = MessageServiceUtils.convertMessageToString(message);
			logger.info(msg);
			sendMessage(msg);
		}
		
	}

	private void sendMessage(String message) {
		try {
			
			companyMessageServiceImpl.processAndSendEmail(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
