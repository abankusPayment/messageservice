package com.abankuspayment.messageservice.listener;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.abankuspayment.messageservice.data.entity.SMSMessageRequest;
import com.abankuspayment.messageservice.service.AuthenticationMessageService;
import com.abankuspayment.messageservice.service.SMSMessageService;
import com.abankuspayment.messageservice.service.utils.MessageServiceUtils;

public class CustomerMessageListener implements MessageListener {
	private static final Logger logger = Logger.getLogger(CustomerMessageListener.class);
	
	@Autowired
	private SMSMessageService smsMessageServiceImpl;
	
	private @Value("${customerPaymentNotification}") String customerPayment;
	
	private @Value("${newCustomerNotification}") String newCustomer;
	
	private @Value("${customerPinNotification}") String customerPin;
	@Override
	public void onMessage(Message message) {
		if(message != null){
			String msg = MessageServiceUtils.convertMessageToString(message);
			logger.info(msg);
			handleMessage(msg);
		}
	}

	private void handleMessage(String msg) {
		SMSMessageRequest request = null;
		if(msg != null && !msg.isEmpty()){
			try {
				request = MessageServiceUtils.convertFromJSON(SMSMessageRequest.class,msg);
				if(request.getRequestType().equalsIgnoreCase(customerPayment)){
					smsMessageServiceImpl.paymentNotification(request);
				}else if(request.getRequestType().equalsIgnoreCase(newCustomer)){
					smsMessageServiceImpl.customerNotification(request);
				}else if(request.getRequestType().equalsIgnoreCase(customerPin)){
					smsMessageServiceImpl.customerPinNotification(request);
				}
			} catch (IOException e) {
				logger.warn(e.getMessage(), e);
			}
		}
		
	}


}
