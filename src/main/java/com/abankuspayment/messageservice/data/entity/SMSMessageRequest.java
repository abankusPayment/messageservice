package com.abankuspayment.messageservice.data.entity;

import java.math.BigDecimal;
import java.util.UUID;

public class SMSMessageRequest {
	
	private String uuid;
	
	private String requestType;
	
	private String smsMessage;
	
	private String phoneNumberTo;
	
	private BigDecimal amountPaid;
	
	private String companyAlias;
	
	private String companyName;

	private Long customerId;
																																																																																																																																																														
	private Long companyId;
	
	private String paymentDate;
	
	private String referenceCode;

	private UUID sentMessageId;
	
	private String employee;
	
	private String customerPinCode;
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getSmsMessage() {
		return smsMessage;
	}

	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}

	public String getPhoneNumberTo() {
		return phoneNumberTo;
	}

	public void setPhoneNumberTo(String phoneNumberTo) {
		this.phoneNumberTo = phoneNumberTo;
	}

	public BigDecimal getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		if(customerId instanceof Long){
			this.customerId = (Long) customerId;
		}else if(customerId instanceof Integer){
			this.customerId = ((Integer)customerId).longValue();
		}
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Object companyId) {
		if(companyId instanceof Long){
			this.companyId = (Long) companyId;
		}else if(companyId instanceof Integer){
			this.companyId = ((Integer)companyId).longValue();
		}
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public UUID getSentMessageId() {
		return sentMessageId;
	}

	public void setSentMessageId(UUID sentMessageId) {
		this.sentMessageId = sentMessageId;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getCompanyAlias() {
		return companyAlias;
	}

	public void setCompanyAlias(String companyAlias) {
		this.companyAlias = companyAlias;
	}

	public String getCustomerPinCode() {
		return customerPinCode;
	}

	public void setCustomerPinCode(String customerPinCode) {
		this.customerPinCode = customerPinCode;
	}
	
	
}
