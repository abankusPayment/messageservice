package com.abankuspayment.messageservice.data.entity;

import java.util.Date;

public class Products {

	private String productUUID;
	
	private int companyId;
	
	private String employeeNumber;
	
	private String productName;
	
	private Date dateCreated;

	public String getProductUUID() {
		return productUUID;
	}

	public void setProductUUID(String productUUID) {
		this.productUUID = productUUID;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
}
