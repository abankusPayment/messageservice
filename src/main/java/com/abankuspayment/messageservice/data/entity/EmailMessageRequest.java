package com.abankuspayment.messageservice.data.entity;

import java.util.UUID;

public class EmailMessageRequest implements MessageRequest{
	
	private UUID uuid;
	
	private String companyName;
	
	private String requestType;
	
	private String emailMessage;
	
	private String emailTo;
	
	private String receiverName;

	private String username;
	
	private String tempPassword;
	
	private String url;
	
	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getEmailMessage() {
		return emailMessage;
	}

	public void setEmailMessage(String emailMessage) {
		this.emailMessage = emailMessage;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public EmailMessageRequest(ForgotUsernameRequest request){
		this.emailTo = request.getEmailAddress();
		this.receiverName = request.getFirstname();
		this.username = request.getUsername();
	}
	
	public EmailMessageRequest(ChangePasswordRequest request){
		this.emailTo = request.getEmailAddress();
		this.receiverName = request.getName();
		this.requestType = "changePassword";
		this.tempPassword = request.getTempPassword();
		this.url = request.getUrl();
	}
	public EmailMessageRequest(EmployeeRegistrationRequest request){
		this.emailTo = request.getEmailAddress();
		this.receiverName = request.getName();
		this.requestType = "activateEmployee";
		this.tempPassword = request.getTempPassword();
		this.url = request.getUrl().concat(request.getVerificationCode());
	}
	
	public EmailMessageRequest(CompanyNotificationMessage request){
		this.emailTo = request.getEmailAddress();
		this.companyName = request.getCompanyName();
	}
	public String getTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
}
