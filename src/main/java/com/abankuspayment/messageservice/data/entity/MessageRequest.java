package com.abankuspayment.messageservice.data.entity;

import java.util.UUID;

public interface MessageRequest {
	
	public String getRequestType();


	public UUID getUuid();



}
