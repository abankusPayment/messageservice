package com.abankuspayment.messageservice.data.entity;

public class CompanyNotificationMessage {

	private String companyName;
	
	private int companyId;
	
	private String emailAddress;
	

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int compantId) {
		this.companyId = compantId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	
	
}
