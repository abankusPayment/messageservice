package com.abankuspayment.messageservice;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.abankuspayment.messageservice.service.EmailMessageService;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("/rs")
public class MessagingResource {
	
	private static final Logger logger = LoggerFactory.getLogger(MessagingResource.class);
	
	@Autowired
	private EmailMessageService emailMessageServiceImpl;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		return "home";
	}
	
	@RequestMapping(value = "/retreivePaymentQueue", method = RequestMethod.GET, consumes="application/json")
	public void collectSQSQueue(){
		
	}

	@Async("executor")
	@RequestMapping(value = "/sendEmailforEmployee", method = RequestMethod.POST, consumes="application/json")
	public void sendEmailforEmployee(@RequestBody String mailRequest){

	}
	
	public MessagingResource() {
		super();
	}
	
}
